    package com.example.holdmyline;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.holdmyline.Common.Common;
import com.example.holdmyline.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import androidx.appcompat.app.AppCompatActivity;

public class signin extends AppCompatActivity {
    EditText EditPhone,editPassword;
    Button btnSignIn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);

        editPassword = (MaterialEditText)findViewById(R.id.editPassword);
        EditPhone= (MaterialEditText)findViewById(R.id.EditPhone);
        btnSignIn=(Button)findViewById(R.id.btnSignIn);

        //Init Firebase
        final FirebaseDatabase database= FirebaseDatabase.getInstance();
        final DatabaseReference table_user=database.getReference("user");
        btnSignIn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){




                final ProgressDialog mDialog=new ProgressDialog(signin.this);
                mDialog.setMessage("Please Wait...");
                mDialog.show();
                if (!TextUtils.isEmpty(EditPhone.getText().toString().trim()) && !TextUtils.isEmpty(editPassword.getText().toString().trim()))

                    table_user.addValueEventListener(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child(EditPhone.getText().toString()).exists()) {
                    mDialog.dismiss();


                    User user = dataSnapshot.child(EditPhone.getText().toString()).getValue(User.class);
                    user.setPhone(EditPhone.getText().toString());
                    if (user.getPassword().equals(editPassword.getText().toString())) {

                       Intent homeIntent=new Intent(signin.this,Home.class);
                        Common.currentUser=user;
                        startActivity(homeIntent);
                        finish();

                    } else {

                        Toast.makeText( signin.this, "Sign In Failed", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    mDialog.dismiss();
                    Toast.makeText(signin.this, "User doesn't exist", Toast.LENGTH_SHORT).show();
                }
            }
            @Override
                   public void onCancelled(DatabaseError databaseError){

            }

            });
        }});
            }

            }





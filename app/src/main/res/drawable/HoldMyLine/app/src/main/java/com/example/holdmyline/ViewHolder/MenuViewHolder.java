package com.example.holdmyline.ViewHolder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.holdmyline.Interface.ItemClickListener;
import com.example.holdmyline.R;

import androidx.recyclerview.widget.RecyclerView;

public class MenuViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    public TextView txtMenuName;
    public ImageView ImageView;

    private ItemClickListener itemClickListener;
    public MenuViewHolder(View itemView){
        super(itemView);

        txtMenuName=(TextView)itemView.findViewById(R.id.menu_name);
        ImageView=(ImageView)itemView.findViewById(R.id.menu_image);

        itemView.setOnClickListener(this);


    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view){
        itemClickListener.onClick(view,getAdapterPosition(),false);

    }
}

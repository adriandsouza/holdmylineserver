package com.example.hmlserver.Common;


import com.example.hmlserver.Model.Request;
import com.example.hmlserver.Model.User;


public class Common {
    public static User currentUser;
    public static Request currentRequest;

    public static final String UPDATE = "Update";
    public static final String DELETE = "Delete";

    public static final int PICK_IMAGE_REQUEST = 71;

    public static final String baseUrl = "https://maps.googleapis.com";

    public static String convertCodeToStatus(String code) {
        if (code.equals("0"))
            return "Placed";
        else if (code.equals("1"))
            return "Being Prepared";
        else
            return "Ready";
    }
}

 //   public static IGeoCoordinates getGeoCodeService() {
   //     return RetrofitClient.getClient(baseUrl).create(IGeoCoordinates.class);
    //}
